# Simple plugin-less automatic session management in vim :D

It seems like when I'm editing a file or set of files and eventually need to
close vim or shut off my computer, I end up reopening the same set of windows
quite often.

I've found [this](https://github.com/rmagatti/auto-session) plugin for neovim
and there's almost definitely others that have similar function, but vim
happens to have built in session management!

Reading vim's help pages, one would find the entry titled
["session-file"](https://vimhelp.org/starting.txt.html#session-file)
that outlines how vim's `:mksession` command and `-S` option function to store
and load the current state of vim.

I used to use neovim as my primary text editor, however I've recently been
learning the things vim can do out of the box including with things that
plugins seem to normally do. 

I personally don't really like the reliance of third-party plugins for
functionality that could easily be implemented my config (in this case it's
only ~20 lines!), so learning a little vimscript (or lua if you're using
neovim) could help implement the features in some plugins that you find
yourself using most often.

First, we're going to need a way to actually save vim's current state. If you
read the `:mksession` sections in the vim help pages, you should know how to
save. The issue is that the default path for a save is "Session.vim" in the
current directory, and I find that ugly. The trick for me was to create my own
sessions directory (akin to `undodir` or `backupdir` to a degree) and somehow
save the sessions there uniquely.

### Vars

I created a variable called `sessiondir` and decided it would be in
`~/.local/shre/vim/session/`, and the file path would be `sessiondir`
concatenated with the current working directory (`getcwd()`) with all the '/'s
replaced with '^'s. Initially it was going to be `%` because that matched
other backup methods that vim uses, however `:mksession` doesn't seem to like
them..

![Vars](/images/Vars.png)

### Saves

Saving a session is fairly easy with these variables. We only need a function
that has `execute("mksession! " . g:sessionfile)` now. The `!` at the end
tells vim to run it anyways and overwrite any session file with the same name.

![SaveSession](/images/SaveSession.png)

### Loads

Since I only want to have the session loaded if I type `vim`, I need to check
if the current buffer (upon entering vim) is empty. The way to do that is
through `strlen(@%) == 0`, as there won't be a name if the buffer is empty. We
also do a check to make sure the file exists before trying to source it (the
session file is just a vimscript file you can load).

To be able to use the function when vim *isn't* just an empty buffer, I gave
the function a parameter to ignore the strlen statement if need be.

![TryLoadSession](/images/TryLoadSession.png)

### Automatic saving/loading

Finally, we need a way to *automatically* save/load the session. Luckily
`autocmd` exists!

![AutoCMD](/images/AutoCMD.png)

And that's it! Session management is fairly simple in vanilla vim and doesn't
even take many lines of code to complete.

## Optional: Ignore some directories.

Sometimes (almost always) you don't want to save the state of vim when you're
in your home directory, so we want to have a way to ignore some directories if
you're automatically saving the state on BufWrite. I'll leave this as more of an exercise (I don't want to write all two of the steps out), though ;).
