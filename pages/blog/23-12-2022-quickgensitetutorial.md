# quick gensite tutorial

Table of Contents:

1. [Initialization]
2. [Viewing your site]
3. [Adding Pages]
4. [Blog Posts]

If you're reading this blogpost at `bubby.me`, you're seeing the output of
`gensite`, a single bash script that I wrote to create the static html for this
site.

The repository for the script can be found
[here](https://codeberg.org/BubbyRoosh/gensite), and you should realistically
only need the script itself.

## Initialization

To create a new website/project, put gensite in a directory and run
`gensite init`. This will create a set of files/directories for you to write
your website. The general structure of the files/dirs is as follows:

1. `config`

    Your main config. It defines your site's name/title and various directories
    for gensite to use. This is also where you define the markdown->html
    command, which is just pandoc by default.

2. `fonts`

    Where your fonts may be stored. This directory will simply be copied over
    when you generate the website. This and the images directory are simply
    to separate the `root` directory from the `output` directory, as cleaning
    the project essentially deletes everything in the `output` directory.

3. `images`

    Similar to the fonts directory but reserved for images.

4. `pages`

    Contains all of your markdown files. The structure is simply an `index.md`
    page in a directory followed by any other pages you want. Any inner
    directories will need an index.md file, as the web server will default to
    one.

5. `site`

    The output directory where the fonts, images, and pages will be
    copied/converted to.

6. `styles.css`
    
    Your general css file for styling the website.

7. `templates`

    The header and footer html for each page. Currently, gensite only supports
    that, but per-page/dir header/footer documents may be added in the future.

Note that most of these locations can be changed in `config` if you don't like
the naming.

![](/images/GenSiteInit.png)

## Viewing your site

Now that you have a basic template set up for your website, you'd *probably*
want to be able to see how it looks so far. This can be done with any http
server, however the simplest one I've found is the built in one that comes with
python. Simply run `python3 -m http.server -d site` to start an http server at
`127.0.0.1:8000`, then open that link in your browser
([link if you're trying it out while reading](http://127.0.0.1:8000))

![](/images/PythonHttpServer.png)

## Adding Pages

Adding a page is as simple as creating a new file in the pages directory (or
wherever you've set your `$INPUT` directory to in your config file). Just
remember to have an `index.md` file in each directory in the pages directory,
as you will probably get a 404 when trying to enter it otherwise. If you need
markdown-related help, you can refer to whatever standard your md->html
converter uses. In the case of pandoc, you can find help
[here](https://pandoc.org/MANUAL.html#pandocs-markdown).

## Blog Posts

The (currently as of writing this post) newest feature to `gensite` is the
insanely janky blogging setup. Because of the simplicity of the whole site, one
could easily implement the same setup by just creating files/dirs in the pages
directory and somehow editing the header/footer and styles.css files to hide
the navigation bar at the top of each page.

Anyways...

Simply run `./gensite blogpost` and any directories/files will be created
automatically. You will be prompted for the name of the post. After entering
the desired name, the post will be put in your blog directory (which is relative
to your input directory) according to your config. If you kept the defaults so
far, this would be in `./pages/blog/`. A list is automatically updated in the
`index.md` file in your blog dir, so that shouldn't need to be touched
(unless you want to change something above the list like a preamble of sorts).

![](/images/BlogPost.png)
