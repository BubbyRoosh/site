# BubbyRoosh

(This website uses prefers-color-scheme for a light/dark mode.)

[codeberg](https://codeberg.org/BubbyRoosh)

## Stuff I use

NOTE: Almost everything here is subject to change as I end up changing my
development environment more than I end up actually developing things..

* Editor: \[neo\]vim

* Terminal:
    * (xorg) [alacritty](https://alacritty.org/)
    * (wayland) [foot](https://codeberg.org/dnkl/foot)

* Window Manager:
    * (xorg) [dwm](https://codeberg.org/BubbyRoosh/dwm/)
    * (wayland) [dwl](https://codeberg.org/BubbyRoosh/dwl/)

	My own build with a couple patches :D.

* OS: [alpine](https://alpinelinux.org)

* Git Host: [codeberg](https://codeberg.org/)

	[I don't like microsoft](https://news.microsoft.com/announcement/microsoft-acquires-github/)

    I primarily use codeberg to host my source code, however github has some (mostly legacy)
    stuff on it as well.
