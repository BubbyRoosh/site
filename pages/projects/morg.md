# morg

![morg](/images/morg.png){ width=50% }

Tool for organizing music by the audio files' metadata.

## Installation

1. Install [Go](https://go.dev/)
2. Install morg `go install codeberg.org/BubbyRoosh/morg@latest`

## Usage

Simply run morg with the paths to all the files you want sorted:
```
morg path/to/file
```

Or...

Scan all files recursively with `find` (takes stdin input separated by '\n'):
```
find -name "*.mp3" | morg
```

Information on command-line arguments can be displayed by passing "--help"
