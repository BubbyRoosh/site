# Gensite

Example:
```
$ git clone https://codeberg.org/BubbyRoosh/gensite
$ cd gensite
$ ./gensite init
$ echo '# Hello :D' > pages/index.md
$ ./gensite gen
```

`gensite` will put the outputted html files for the http server in `site` by default.

If you want a tutorial, read [this](/blog/23-12-2022-quickgensitetutorial.html) :)
