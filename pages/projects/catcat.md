# catcat :D

This was a fun little project. It's the unix `cat` command, but the code's shaped like a cat! :D

The only warning that *might* show up is due to libc `#include`s (`<sys/errno.h>` vs `<errno.h>`)

Supports -n (number) and -s (squeeze) options.

If you're too lazy to just go to the [repository](https://codeberg.org/BubbyRoosh/catcat), here's the cat code:

```c
#include <sys/errno.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "argp.h"

    void      cat
   (FILE*    f,int
   num,int squeeze)
  {unsigned cur,last,i;
 char /* >:3 */ *line =
  NULL;size_t linecap=0
  ;ssize_t len;cur =last
    = i = 0;while((len =                         getline
        (&line, &linecap, f)) > 0) { i++; if (squeeze) { cur =
        len == 1; if (cur && last) continue; last = cur;}if (num)
       printf("%-8d%s", i, line);else printf("%s", line);}free(line);
        }int main(int argc, char **argv) { int i, nflag, sflag       ;
        FILE *fp; nflag = sflag = 0;ARGBEGIN {case 'n':nflag++        ;      ;
        break;case 's':sflag++;break ; default: fprintf(stderr         ,   //
        "%s [-ns] [file ...]\n"                 , progname)             ;;;
          return 1;}                              ARGEND;
           if(argc                                 ==0){
           cat(stdin                               , nflag,
           sflag) ;                                }else{
            for(i                                  =0;i<
            argc;                                  i++){
            if (!                                  strcmp
         (argv[i]                                ,"-"))fp
    = stdin;else fp = fopen(argv[i], "r");if (fp != NULL) cat(fp,
    nflag, sflag);else err(errno, "couldn't open \'%s\'", argv[i]
    );fclose(fp);}}}/***************** ROOMBA ******************/
```
