# guhnoo

## Philosophy

GNU only recommends distributions that offer **only** free software. [Non-free
software isn't even an option to the
FSF](https://www.gnu.org/distros/optionally-free-not-enough.html). This sounds
reasonable at first glance, however it seems ironic that allowing user freedom
includes disallowing many programs and firmware to even be installed.

What is this video even..? It's on the main page of [gnu.org](https://gnu.org).
I assume it's for the purpose of seeing the impracticalities of non-free
software, but the method in which it's conveyed draws much more attention to
the "cool graphics" and action instead of the theme of the video (in my opinion
at least).
[larp!](https://static.gnu.org/nosvn/videos/fight-to-repair/videos/Fight-to-Repair-720p.webm)

## Code

The following examples are just to show the complications GNU code has and how
it can affect a program's readability (what the f\*ck is the coding style???).
And possibly (usually not somehow) performance.

This [libc comparison](http://www.etalabs.net/compare_libcs.html) shows the
differences between musl, uClibc, dietlibc, and glibc. Essentially green =
good, orange/yellow = meh, and red = bad. Just looking at the first 3
categories, it can be seen that glibc is much.. ***much*** larger in size, and
seems to have many issues with resource exhaustion compared to musl.

While the performance is usually better for glibc, the actual code is
[horrendous](https://sourceware.org/git/?p=glibc.git;a=blob;f=string/strlen.c;h=0b8aefb81281a7a020351864ab21ddbcfe0f3ebb;hb=HEAD).
Comparing to [musl's
implementation](https://git.musl-libc.org/cgit/musl/tree/src/string/strlen.c),
it should be fairly obvious how much nicer musl's code is when compared glibc.
Going out of linux libcs specifically, [OpenBSD's
implementation](https://github.com/openbsd/src/blob/master/lib/libc/string/strlen.c)
also shows how simple the code needs to be. Note this is essentially the same
as musl's, just without the \_\_GNUC\_\_ compatability macro(?) musl has.

[echo comparison](https://gist.github.com/fogus/1094067)

![cat](/images/longcat.png)
